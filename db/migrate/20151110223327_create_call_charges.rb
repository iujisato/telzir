class CreateCallCharges < ActiveRecord::Migration
  def change
    create_table :call_charges do |t|
      t.integer :origin
      t.integer :destination
      t.integer :time

      t.timestamps null: false
    end
  end
end
