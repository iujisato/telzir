class CreatePossibleCharges < ActiveRecord::Migration
  def change
    create_table :possible_charges do |t|
      t.integer :call_charge_id
      t.integer :plan_id

      t.timestamps null: false
    end
  end
end
