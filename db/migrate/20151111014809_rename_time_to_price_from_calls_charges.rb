class RenameTimeToPriceFromCallsCharges < ActiveRecord::Migration
  def change
    rename_column :call_charges, :time, :price
    change_column :call_charges, :price, :money
  end
end
