class ChangeDatatype < ActiveRecord::Migration
  def change
    change_column :call_charges, :price, :decimal, :precision => 8, :scale => 2
  end
end
