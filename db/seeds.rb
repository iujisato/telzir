Plan.find_or_create_by(name: "Fale Mais 30", description: "Com o Fale Mais 30 você fala até 30 minutos com qualquer telefone do brasil sem pagar nada!", time_credit: 30)
Plan.find_or_create_by(name: "Fale Mais 60", description: "Com o Fale Mais 60 você tem mais vantagens, fale até 60 minutos com qualquer telefone do brasil sem pagar nada!", time_credit: 60)
Plan.find_or_create_by(name: "Fale Mais 120", description: "Com o Fale Mais 120 você tem muito mais vantagens, fale até 120 minutos com qualquer telefone do brasil sem pagar nada!", time_credit: 120)
CallCharge.find_or_create_by(origin: 11, destination: 16, price: 1.9)
CallCharge.find_or_create_by(origin: 16, destination: 11, price: 2.9)
CallCharge.find_or_create_by(origin: 11, destination: 17, price: 1.7)
CallCharge.find_or_create_by(origin: 17, destination: 11, price: 2.7)
CallCharge.find_or_create_by(origin: 11, destination: 18, price: 0.9)
CallCharge.find_or_create_by(origin: 18, destination: 11, price: 1.9)

call_charges = CallCharge.all
call_charges.each do |call|
  call.plans << Plan.all
  call.save
end
