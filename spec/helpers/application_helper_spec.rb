require 'rails_helper'

RSpec.describe ApplicationHelper do
  describe ".origins" do
    it "returns distincts origins in array" do
      2.times do
        CallCharge.create!(origin: 11)
      end
      CallCharge.create!(origin: 16)
      CallCharge.create!(origin: 17)
      CallCharge.create!(origin: 18)
      
      expect(helper.origins).to match_array([11, 16, 17, 18])    
    end
  end

 describe ".destinations" do
    
    it "returns empty if the given argument is nil" do
      2.times do
        CallCharge.create!(destination: 11)
      end
      CallCharge.create!(destination: 16)
      CallCharge.create!(destination: 17)
      CallCharge.create!(destination: 18)
      
      expect(helper.destinations).to be_empty    
    end

    it "returns distincts destinations in array if params is not nil" do
      2.times do
        CallCharge.create!(origin: 11, destination: 16)
      end
      CallCharge.create!(origin: 11, destination: 17)
      CallCharge.create!(origin: 11, destination: 18)
      CallCharge.create!(origin: 16, destination: 11)
      
      expect(helper.destinations(11)).to match_array([16, 17, 18])    
    end
  end  

end
