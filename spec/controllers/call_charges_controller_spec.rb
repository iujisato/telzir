require "rails_helper"

RSpec.describe CallChargesController do
  describe "Get /calculate" do
    it "Calculate charge should receive the plans and respond with correct prices" do
      Plan.create(time_credit: 30)
      Plan.create(time_credit: 60)
      Plan.create(time_credit: 120)
      CallCharge.create!(origin: 11, destination: 16, price: 1.9, plans: Plan.all)
      get :calculate_charge, origin: 11, destination: 16, time_spent: 20
      expect(assigns(:calculate_charge)).to eq([0, 0, 0])    
    end

      it "Calculate charge should receive the plans and respond with correct prices" do
      Plan.create(time_credit: 30)
      Plan.create(time_credit: 60)
      Plan.create(time_credit: 120)
      CallCharge.create!(origin: 11, destination: 16, price: 1.9, plans: Plan.all)
      get :calculate_charge, origin: 11, destination: 16, time_spent: 120
      expect(assigns(:calculate_charge)).to eq([188.10, 125.40, 0])    
    end
  end

end
