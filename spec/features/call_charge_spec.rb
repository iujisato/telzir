require 'rails_helper'

feature "Telzir plans", :js do
  scenario "User can calculate call charges with different plans" do
    plan_fale_mais = Plan.create!(name: 'Fale Mais 30', time_credit: 30)
    plan_fale_mais = Plan.create!(name: 'Fale Mais 60', time_credit: 60)
    plan_fale_mais = Plan.create!(name: 'Fale Mais 120', time_credit: 120)
    call_charges = CallCharge.create!(origin: 11, destination: 16, price: 1.90, plans: Plan.all)
    visit root_path
    select('11', :from => 'origin')
    select('16', :from => 'destination', match: :first)
    fill_in('time_spent', :with => '120')
    click_button('calcular')
    expect(page).to have_css('.without_fale_mais', text: "228.00")
    expect(page).to have_css('.fale_mais_30', text: "188.10")
    expect(page).to have_css('.fale_mais_60', text: "125.40")
    expect(page).to have_css('.fale_mais_120', text: "0")
  end
end
