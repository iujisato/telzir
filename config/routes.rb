Rails.application.routes.draw do


  resources :plans, only: [:show]
  get '/update_destination' => "call_charges#update_destination", :as => 'update_destination'
  get '/calculate' => "call_charges#calculate_charge", as: "calculate" 
  root 'call_charges#show'

end