module ApplicationHelper

  def to_snake_case(name)
    if name.present?
      name.downcase.split.join("_")
    end
  end

  def origins
    CallCharge.distinct.pluck(:origin).reject(&:nil?)
  end

  def destinations(origin = nil)
    return [] if origin.blank?
    CallCharge.distinct.where(origin: origin).pluck(:destination).reject(&:nil?)
  end

  def request_params
    request.params.reject { |k, v| ['commit', 'controller', 'action', 'utf8'].include? k }
  end

end
