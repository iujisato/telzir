class CallChargesController < ApplicationController

  def show
    @plans = Plan.all
  end

  def update_destination
    call_destination = CallCharge.where(origin: params[:origin])
    @call_destination = call_destination.map(&:destination)
   
    render :update_destination, layout: false 
  end

  def calculate_charge
    if params[:origin].blank? || params[:destination].blank?
       
      redirect_to root_path, alert: "Você precisa preencher todos os campos"
    else
      @plans = Plan.all
      calculate_charge_without_plan
      calculate_charge_with_plan
      render :show
    end
    
  end

  private
  
  def calculate_charge_without_plan
    calculate = CallCharge.find_by(origin: params[:origin], destination: params[:destination])
    @charge_without_plan = (params[:time_spent].to_f * calculate.price).round(2)
  end

  def calculate_charge_with_plan
    set_call_charge = CallCharge.find_by(origin: params[:origin], destination: params[:destination])
    @calculate_charge = set_call_charge.plans.map do |plan|
      time_charged = params[:time_spent].to_i - plan.time_credit
      unless time_charged > 0
        time_charged = 0
      end
      (time_charged * (set_call_charge.price.to_f * 1.1)).round(2)  
    end
  end

end
