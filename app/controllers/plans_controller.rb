class PlansController < ApplicationController

def show
  @plan = Plan.find(params[:id])
  calculate_charge_without_plan
  calculate_charge_with_plan
end

private

  def calculate_charge_without_plan
    calculate = CallCharge.find_by(origin: params[:origin], destination: params[:destination])
    @charge_without_plan = (params[:time_spent].to_f * calculate.price).round(2).to_s
  end

  def calculate_charge_with_plan
    set_call_charge = CallCharge.find_by(origin: params[:origin], destination: params[:destination])
    time_charged = params[:time_spent].to_i - @plan.time_credit
      unless time_charged > 0
        time_charged = 0
      end
    @charge_with_plan = (time_charged * (set_call_charge.price.to_f * 1.1)).round(2).to_s  
  end

end
