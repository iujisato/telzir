class CallCharge < ActiveRecord::Base
  has_many :possible_charges
  has_many :plans, :through => :possible_charges
end
