class Plan < ActiveRecord::Base
  has_many :possible_charges
  has_many :call_charges, :through => :possible_charges
end
