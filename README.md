# Telzir - Call charges calculator
Telzir's call charge calculator that lets the client to fully know which plan is the best for him.

## Requirements
- Ruby 2.2.1
- Rails 4.2.3
- PostGres 9.3.10

## Deploy
Deploy is made using Heroku. 
Check out the live app! 
https://telzir-calculator.herokuapp.com